import 'package:bitcoin/screens/Price_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(Bitcoin());
}

class Bitcoin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: PriceScreen(),
    );
  }
}
